""" Service for interacting with the Google Drive API. """
# pylint: disable=no-member disable=line-too-long

from logging import getLogger

from google.oauth2.credentials import Credentials
from googleapiclient.discovery import build
from googleapiclient.http import MediaFileUpload

logger = getLogger(__name__)


class GoogleDriveService:
    """ Service for interacting with the Google Drive API. """
    def __init__(self, token_file: str = "token.json"):
        creds = Credentials.from_authorized_user_file(token_file, 'https://www.googleapis.com/auth/drive.file')
        self.service = build('drive', 'v3', credentials=creds)

    def upload_file(self, file_path: str, title: str, mimetype: str = 'text/tab-separated-values'):
        """ Upload a file to Google Drive. """
        file_metadata = {
            'name': title,
            'mimeType': 'text/tab-separated-values'
        }
        media = MediaFileUpload(file_path,
                                mimetype='text/tab-separated-values',
                                resumable=True)
        file = self.service.files().create(body=file_metadata,
                                           media_body=media,
                                           fields='id').execute()
        logger.info("File uploaded to Google Drive with ID %s", file['id'])
