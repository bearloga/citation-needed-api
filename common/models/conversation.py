from pydantic import BaseModel

from app.models.parameters import ChatGPTRequestInfo, ChatGPTResponse
from app.models.search.search import (GoogleSearchResult,
                                      WikipediaSearchResult)
import logging

logger = logging.getLogger(__name__)


class Conversation(BaseModel):
    conversation_id: str | None = None
    request: ChatGPTRequestInfo | None = None
    google_search_result: GoogleSearchResult | None = None
    wiki_search_result: WikipediaSearchResult | None = None
    response: ChatGPTResponse | None = None

    @property
    def user_id(self) -> str | None:
        if self.request and self.request.user_info:
            return self.request.user_info.openai_ephemeral_user_id
        return None

    @property
    def original_user_input(self) -> str | None:
        if self.request:
            return self.request.request.original_user_input
        return None

    @property
    def query_language(self) -> str | None:
        if self.request:
            return self.request.request.query_language
        return None

    @property
    def parsed_query(self) -> str | None:
        if self.request:
            return self.request.request.query
        return None

    @property
    def wiki_search_result_urls(self) -> list[str] | None:
        if self.wiki_search_result:
            return [result.url for result in self.wiki_search_result.parsed_results]
        return None

    @property
    def response_wiki_urls(self) -> list[str] | None:
        if self.response:
            return [result.url for result in self.response.wikipedia_articles_relevant_to_query]
        return None

    @staticmethod
    def relevant_keys() -> list[str]:
        return [
            "conversation_id",
            "user_id",
            "original_user_input",
            "query_language",
            "parsed_query",
            "wiki_search_result_urls",
            "response_wiki_urls",
        ]

    @property
    def relevant_properties(self) -> dict:
        return {key: getattr(self, key) for key in Conversation.relevant_keys()}

    def print_basic_info(self):
        for key, value in self.relevant_properties.items():
            logger.info(f"{key}: {value}")
