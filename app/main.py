""" Main entrypoint for the application. """
import uvicorn
from fastapi import FastAPI

from app.config.server_config import ServerConfigurer

# Logs must be configured before initializing the app
configurer = ServerConfigurer()
configurer.configure_logging()

app = FastAPI()
configurer.configure_app(app)


def main():
    """Launched with `poetry run plugin` only"""
    uvicorn.run("app.main:app", host="0.0.0.0", port=8000, reload=False, workers=5)


if __name__ == "__main__":
    main()
