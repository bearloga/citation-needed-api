""" XTools service implementation for detail lookups on Wikipedia articles"""

import requests
from app.models.parameters import ArticleDetailsResult
import structlog
import urllib

from app.constants import USER_AGENT
from app.services.interfaces import ArticleDetailsService

logger = structlog.getLogger(__name__)


class XtoolsArticleDetailsService(ArticleDetailsService):
    BASE_URL_TEMPLATE = "https://xtools.wmcloud.org/api/page/{}/{}.wikipedia.org/{}?format=json"
    HEADERS = {'User-Agent': USER_AGENT}

    def get_details(self, page_title: str, search_language: str = "en") -> ArticleDetailsResult:
        """ This function retrieves details for a given article. """
        parsed_page_title = urllib.parse.quote(page_title)
        article_info_url = self.BASE_URL_TEMPLATE.format("articleinfo", search_language, parsed_page_title)
        article_info_resp = requests.get(article_info_url, timeout=100, headers=self.HEADERS)
        article_info_resp.raise_for_status()
        article_info = article_info_resp.json()

        link_url = self.BASE_URL_TEMPLATE.format("links", search_language, parsed_page_title)
        link_resp = requests.get(link_url, timeout=100, headers=self.HEADERS)
        link_resp.raise_for_status()
        links = link_resp.json()

        return ArticleDetailsResult(editors=article_info['editors'], 
                                    last_update=article_info['modified_at'], 
                                    outgoing_links=links['links_ext_count'])