""" XTools service implementation for detail lookups on Wikipedia articles"""

import requests
from app.models.parameters import ArticleDetailsResult
import structlog
import urllib

from app.constants import USER_AGENT
from app.services.interfaces import ArticleDetailsService

logger = structlog.getLogger(__name__)


class MockArticleDetailsService(ArticleDetailsService):

    def get_details(self, page_title: str, search_language: str = "en") -> ArticleDetailsResult:
        """ This function retrieves details for a given article. """
        return ArticleDetailsResult(editors=42, 
                                    last_update="23.02.2024", 
                                    outgoing_links=42)