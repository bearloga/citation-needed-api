from app.services.errors import LLMInteractionError
from app.services.interfaces import ArticleDetailsService, FeatureFlaggingService, LLMService, SearchService
from structlog import getLogger
from typing import Any, List, Tuple

from app import constants
from app.utils.settings_types import  SearchStrategy, SystemEnum
from app.models.parameters import FactCheckerRequestInfo
import json

logger = getLogger(__name__)


class ExperimentalCitationFinderService:
    """Fetches articles from Wikipedia and interacts with ChatGPT to find citations for a given query."""
    def __init__(self,
                 search_strategy: SearchStrategy,
                 llm_service: LLMService,
                 search_service: SearchService,
                 article_details_service: ArticleDetailsService,
                 feature_flagging_service: FeatureFlaggingService,
                 instruction_keywords: str | None = None,
                 instruction_sections: str | None = None,
                 instruction_quote: str | None = None):
        self.search_strategy = search_strategy
        self.llm_service = llm_service
        self.search_service = search_service
        self.article_details_service = article_details_service
        self.instruction_keywords = instruction_keywords
        self.instruction_sections = instruction_sections
        self.instruction_quote = instruction_quote
        self.feature_flagging_service = feature_flagging_service

    def select_sections_from_claim(self, 
                                   info: FactCheckerRequestInfo, 
                                   overwrite_model: str | None = None) -> Any:
        self.llm_service.set_search_language("en")

        # try:
        keyword_result = self.llm_service.extract_keyword(selection=info.selection, 
                                                            instructions=self.instruction_keywords,
                                                            overwrite_model=overwrite_model)
        
        search_term = " ".join(keyword_result.keywords)

        wiki_search_results = self.search_service.search_articles(search_term=search_term, search_language="en")
        
        parsed_articles = wiki_search_results.parsed_results

        if (parsed_articles == None or len(parsed_articles) == 0):
            return {"result": "no_articles", "explanation": "No matching Wikipedia pages found."}

        article_titles = []
        for article in parsed_articles:
            article_titles.append(f'"{article.title}"')

        tocs = self.search_service.fetch_tables_of_content(articles=parsed_articles, keywords=keyword_result.keywords, search_language="en")

        selected_sections_result = self.llm_service.select_sections(tocs=tocs, 
                                                                    selection=info.selection,
                                                                    instructions=self.instruction_sections,
                                                                    overwrite_model=overwrite_model)
        
        return {"keywords": keyword_result.keywords, "sections": selected_sections_result.sections}

    def select_sections_from_claim_no_section_selection_step(self, 
                                                             info: FactCheckerRequestInfo, 
                                                             overwrite_model: str | None = None) -> Any:
        self.llm_service.set_search_language("en")

        # try:
        keyword_result = self.llm_service.extract_keyword(selection=info.selection, 
                                                            instructions=self.instruction_keywords,
                                                            overwrite_model=overwrite_model)
        search_terms = keyword_result.keywords
        if keyword_result.search_terms != None:
            search_terms = keyword_result.search_terms

        articles = self.search_service.search_articles(search_term=keyword_result.search_terms[0])

        combined_articles = articles.parsed_results
        # deduplicate the articles - 
        unique_articles = []
        for article in combined_articles:
            # check articles by title
            if article.title not in [a.title for a in unique_articles]:
                unique_articles.append(article) 

        if (unique_articles == None or len(unique_articles) == 0):
            return {"result": "no_articles", "explanation": "No matching Wikipedia pages found."}
        
        article_titles = []
        for article in unique_articles:
            article_titles.append(f'"{article.title}"')

        combined_keywords = None

        # append synonyms to the keywords if any were found
        if keyword_result.synonyms != None:
            combined_keywords = keyword_result.keywords + keyword_result.synonyms + search_terms
        else:
            combined_keywords = keyword_result.keywords + search_terms
        # Remove any duplicates in the combined keywords
        combined_keywords = list(set(combined_keywords))

        tocs = self.search_service.fetch_tables_of_content(articles=unique_articles, keywords=combined_keywords, search_language="en")

        # flatten the list(tuples(str, list(tuple(str, str))) into a simple list(tuple(str, str, str))
        flat_tocs = []
        for article, sections in tocs:
            for section_title, section_keyword_value in sections:
                flat_tocs.append((article, section_title, section_keyword_value))

        # create a list of the top 3 most relevant sections. We do this by looking at the number of times the keyword appears in the section
        sorted_tocs = sorted(flat_tocs, key=lambda x: x[2], reverse=True)
        # remove the keyword_values from the list
        sorted_tocs = [(x[0], x[1]) for x in sorted_tocs]

        sections=sorted_tocs[:3]
        
        return {"keywords": keyword_result.keywords, "sections": sections}
    
    def find_quote_from_sections(self, 
                                 info: FactCheckerRequestInfo, 
                                 selected_sections: List[Tuple[str, str]],
                                 overwrite_model: str | None = None) -> Any:
        self.llm_service.set_search_language("en")

        sections = self.search_service.fetch_sections(sections=selected_sections, search_language="en")

        selected_quote_result = self.llm_service.select_quote(sections=sections, 
                                                                selection=info.selection,
                                                                instructions=self.instruction_quote,
                                                                overwrite_model=overwrite_model)
        selected_quote = selected_quote_result.quote

        if (selected_quote == None):
            return {"result": "no_result", "explanation": "No matching citations found."}
        
        selected_page_title = selected_quote[0]
        article_details = self.article_details_service.get_details(page_title=selected_page_title, search_language="en")

        result_article = self.search_service.create_result_article(selected_quote, article_details=article_details)

        # if we are allowed to show a fact check result and explanation, we do so
        if self.feature_flagging_service.show_fact_check_result() \
            and self.feature_flagging_service.show_fact_check_explanation() \
                and selected_quote_result.result != None \
                and selected_quote_result.explanation != None:
            result_json = {"result": selected_quote_result.result, 
                            "explanation": selected_quote_result.explanation,
                            "article": result_article.dict()}
        # if we are only allowed to show a fact check result, we do so
        elif self.feature_flagging_service.show_fact_check_result() and selected_quote_result.result != None:
            result_json = {"result": selected_quote_result.result, 
                            "article": result_article.dict()}
        else:
            result_json = {"result": "quote_found", 
                            "explanation": "A matching citation was found in an article", 
                            "article": result_article.dict()}
            
        return result_json

    def find_citation(self, 
                      info: FactCheckerRequestInfo,
                      overwrite_model: str | None = None) -> Any:

        logger.info("User request received",
                    source=SystemEnum.User,
                    destination=SystemEnum.CitationNeededAPI,
                    info=info.dict())
        
        selected_sections = self.select_sections_from_claim(info,
                                                            overwrite_model=overwrite_model)["sections"]
        
        sections = self.search_service.fetch_sections(sections=selected_sections, search_language="en")

        selected_quote_result = self.llm_service.select_quote(sections=sections, 
                                                                selection=info.selection,
                                                                instructions=self.instruction_quote,
                                                                overwrite_model=overwrite_model)
        selected_quote = selected_quote_result.quote

        if (selected_quote == None):
            return {"result": "no_result", "explanation": "No matching citations found."}
        
        selected_page_title = selected_quote[0]
        article_details = self.article_details_service.get_details(page_title=selected_page_title, search_language="en")

        result_article = self.search_service.create_result_article(selected_quote, article_details=article_details)

        # if we are allowed to show a fact check result and explanation, we do so
        if self.feature_flagging_service.show_fact_check_result() \
            and self.feature_flagging_service.show_fact_check_explanation() \
                and selected_quote_result.result != None \
                and selected_quote_result.explanation != None:
            result_json = {"result": selected_quote_result.result, 
                            "explanation": selected_quote_result.explanation,
                            "article": result_article.dict()}
        # if we are only allowed to show a fact check result, we do so
        elif self.feature_flagging_service.show_fact_check_result() and selected_quote_result.result != None:
            result_json = {"result": selected_quote_result.result, 
                            "article": result_article.dict()}
        else:
            result_json = {"result": "quote_found", 
                            "explanation": "A matching citation was found in an article", 
                            "article": result_article.dict()}
            
        return result_json
        
    def find_citation_no_section_step(self,
                                      info: FactCheckerRequestInfo,
                                      overwrite_model: str | None = None) -> Any:

        logger.info("User request received",
                    source=SystemEnum.User,
                    destination=SystemEnum.CitationNeededAPI,
                    info=info.dict())
        
        selected_sections = self.select_sections_from_claim_no_section_selection_step(info,
                                                                                      overwrite_model=overwrite_model)["sections"]
        
        sections = self.search_service.fetch_sections(sections=selected_sections, search_language="en")

        selected_quote_result = self.llm_service.select_quote(sections=sections, 
                                                                selection=info.selection,
                                                                instructions=self.instruction_quote,
                                                                overwrite_model=overwrite_model)
        selected_quote = selected_quote_result.quote

        if (selected_quote == None):
            return {"result": "no_result", "explanation": "No matching citations found."}
        
        selected_page_title = selected_quote[0]
        article_details = self.article_details_service.get_details(page_title=selected_page_title, search_language="en")

        result_article = self.search_service.create_result_article(selected_quote, article_details=article_details)

        # if we are allowed to show a fact check result and explanation, we do so
        if self.feature_flagging_service.show_fact_check_result() \
            and self.feature_flagging_service.show_fact_check_explanation() \
                and selected_quote_result.result != None \
                and selected_quote_result.explanation != None:
            result_json = {"result": selected_quote_result.result, 
                            "explanation": selected_quote_result.explanation,
                            "article": result_article.dict()}
        # if we are only allowed to show a fact check result, we do so
        elif self.feature_flagging_service.show_fact_check_result() and selected_quote_result.result != None:
            result_json = {"result": selected_quote_result.result, 
                            "article": result_article.dict()}
        else:
            result_json = {"result": "quote_found", 
                            "explanation": "A matching citation was found in an article", 
                            "article": result_article.dict()}
            
        return result_json