from typing import Any, Dict, List, Tuple

from pydantic import BaseModel

from app.models.search.wiki_search import WikiPage
from app.models.wikipedia_article import WikipediaArticle
from app.utils.settings_types import LLMProviderEnum, SearchProviderEnum

class KeywordExtactionMetadata(BaseModel):
    """ This class contains metadata about a keyword extraction service """
    term: str
    language: str
    provider: LLMProviderEnum
    model: str

class KeywordExtractionResult(BaseModel):
    """ This class represents a keyword extraction result """
    keywords: List[str] | None
    synonyms: List[str] | None
    search_terms: List[str] | None
    unparsed_return_value: str | None
    metadata: KeywordExtactionMetadata
    error: str | None

class SectionSelectionMetadata(BaseModel):
    """ This class contains metadata about a section selection service """
    tocs: Dict[str, List[str]]
    parsed_tocs: str
    language: str
    provider: LLMProviderEnum
    model: str

class SectionSelectionResult(BaseModel):
    """ This class represents a section selection result """
    sections: List[Tuple[str, str]] | None
    unparsed_return_value: str | None
    metadata: SectionSelectionMetadata
    error: str | None

class QuoteSelectionMetadata(BaseModel):
    """ This class contains metadata about a quote selection service """
    sections: List[Tuple[str, str, str]]
    language: str
    provider: LLMProviderEnum
    model: str

class QuoteSelectionResult(BaseModel):
    """ This class represents a quote selection result """
    quote: Tuple[str, str, str] | None
    no_quote_selected: bool
    unparsed_return_value: str | None
    metadata: QuoteSelectionMetadata
    error: str | None
    result: str | None
    explanation: str | None