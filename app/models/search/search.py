from typing import Any

from pydantic import BaseModel

from app.models.search.wiki_search import WikiPage
from app.models.wikipedia_article import WikipediaArticle
from app.utils.settings_types import SearchProviderEnum

class SearchMetadata(BaseModel):
    """ This class contains metadata about a search service """
    term: str
    language: str
    provider: SearchProviderEnum


class SearchResult(BaseModel):
    """ This class represents a search result """
    original_results: list[Any]
    parsed_results: list[WikipediaArticle]
    metadata: SearchMetadata


class WikipediaSearchResult(SearchResult):
    original_results: list[WikiPage]
