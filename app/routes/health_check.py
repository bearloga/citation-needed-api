from app.utils.settings import get_settings
from fastapi import APIRouter

router = APIRouter()


@router.get("/", include_in_schema=False)
async def hello_world():
    """ Hello World endpoint for sanity checking """
    settings = get_settings()
    return settings.greeting_message
