FROM python:3.11-slim-buster

RUN useradd -m myuser

WORKDIR /app

COPY pyproject.toml poetry.lock ./
RUN pip install poetry \
    && poetry config virtualenvs.create false \
    && poetry install --no-root

COPY . ./

# do not remove the below lines for now.
# there is some issue with the kaniko build, so the files are not geting correct permissions
RUN ls -altrh /usr/local/bin
RUN /usr/local/bin/uvicorn --version

RUN chown -R myuser:myuser /app

USER myuser

ENV LOG_JSON_FORMAT=1

CMD ["/usr/local/bin/uvicorn", "app.main:app", "--host", "0.0.0.0", "--log-config", "/app/app/config/uvicorn_disable_logging.json", "--workers", "5"]
