Accuracy: 0.52

Incorrect Results:

Claim: Frank Sinatra's middle name was Albert.
Label: SUPPORTS
Result: NOT ENOUGH INFO
Result details: {'result': 'no_result', 'explanation': 'No matching citations found.'}

Claim: Star Wars: The Force Awakens received only praise.
Label: REFUTES
Result: SUPPORTS
Result details: {'result': 'partially_correct', 'explanation': 'While the majority of critics praised the film, there were some negative reviews and criticisms, such as comparisons to previous films and accusations of lacking originality.', 'article': {'url': 'http://en.wikipedia.org/?curid=14723194', 'page_url': 'http://en.wikipedia.org/?curid=14723194', 'section_url': 'http://en.wikipedia.org/?curid=14723194#Critical_response', 'title': 'Star Wars: The Force Awakens', 'section': 'Critical response', 'snippet': 'Star Wars: The Force Awakens received overwhelmingly positive reviews from critics.', 'references': 712, 'contributors': 2496, 'last_updated': '2024-03-11 13:38'}}

Claim: Samuel L. Jackson was not in the M. Night Shyamalan film Unbreakable.
Label: SUPPORTS
Result: REFUTES
Result details: {'result': 'incorrect', 'explanation': 'The claim is incorrect as Samuel L. Jackson did appear in the film Unbreakable as the character Elijah Price/Mr. Glass.', 'article': {'url': 'http://en.wikipedia.org/?curid=32252', 'page_url': 'http://en.wikipedia.org/?curid=32252', 'section_url': 'http://en.wikipedia.org/?curid=32252#Cast', 'title': 'Unbreakable (film)', 'section': 'Cast', 'snippet': 'Samuel L. Jackson as Elijah Price/Mr. Glass, a comic book theorist, and deranged domestic terrorist with brittle bone disease', 'references': 65, 'contributors': 1358, 'last_updated': '2024-03-16 07:17'}}

Claim: Planet of the Apes' filming took place between May 21 and August 10, 1967.
Label: NOT ENOUGH INFO
Result: SUPPORTS
Result details: {'result': 'correct', 'explanation': "The quote directly confirms that the filming of 'Planet of the Apes' took place between May 21 and August 10, 1967, supporting the claim as correct.", 'article': {'url': 'http://en.wikipedia.org/?curid=18618306', 'page_url': 'http://en.wikipedia.org/?curid=18618306', 'section_url': 'http://en.wikipedia.org/?curid=18618306#Filming', 'title': 'Planet of the Apes (1968 film)', 'section': 'Filming', 'snippet': 'Filming began on May 21, 1967, and wrapped on August 10.', 'references': 99, 'contributors': 1452, 'last_updated': '2024-03-10 18:43'}}

Claim: Gillian Anderson was born in Somalia to Somali parents.
Label: REFUTES
Result: NOT ENOUGH INFO
Result details: {'result': 'no_result', 'explanation': 'No matching citations found.'}

Claim: Red Hot Chili Peppers were originally named Tony Flow and the Miraculously Majestic Masters of Mayhem.
Label: SUPPORTS
Result: NOT ENOUGH INFO
Result details: {'result': 'no_result', 'explanation': 'No matching citations found.'}

Claim: Italy experienced economic growth between 1494 to 1559.
Label: NOT ENOUGH INFO
Result: REFUTES
Result details: {'result': 'incorrect', 'explanation': 'The passage discusses the power dynamics in Italy after 1559, but it does not mention economic growth during the period from 1494 to 1559.', 'article': {'url': 'http://en.wikipedia.org/?curid=239500', 'page_url': 'http://en.wikipedia.org/?curid=239500', 'section_url': 'http://en.wikipedia.org/?curid=239500#Interpretations', 'title': 'Italian Wars', 'section': 'Interpretations', 'snippet': 'In the long-term, Habsburg primacy in Italy continued to exist, but it varied significantly due to the change of dynasties in Austria and Spain.', 'references': 34, 'contributors': 594, 'last_updated': '2024-03-16 03:14'}}

Claim: Inhumans is developed only for ABC.
Label: REFUTES
Result: SUPPORTS
Result details: {'result': 'correct', 'explanation': 'The statement is correct as Inhumans was developed in conjunction with ABC Studios and aired on ABC.', 'article': {'url': 'http://en.wikipedia.org/?curid=31968788', 'page_url': 'http://en.wikipedia.org/?curid=31968788', 'section_url': 'http://en.wikipedia.org/?curid=31968788#Entertainment_division', 'title': 'Marvel Television', 'section': 'Entertainment division', 'snippet': 'In November 2016, Marvel Television and IMAX Corporation announced Inhumans, to be produced in conjunction with ABC Studios, and to air on ABC.', 'references': 137, 'contributors': 317, 'last_updated': '2024-03-06 19:47'}}

Claim: A Nightmare on Elm Street stars Katie Cassidy and Rooney Mara.
Label: NOT ENOUGH INFO
Result: REFUTES
Result details: {'result': 'incorrect', 'explanation': 'Katie Cassidy plays the role of Kris Fowles, not Rooney Mara. Rooney Mara plays the role of Nancy Holbrook.', 'article': {'url': 'http://en.wikipedia.org/?curid=23286957', 'page_url': 'http://en.wikipedia.org/?curid=23286957', 'section_url': 'http://en.wikipedia.org/?curid=23286957#Casting', 'title': 'A Nightmare on Elm Street (2010 film)', 'section': 'Casting', 'snippet': 'Rooney Mara plays the role of Nancy Holbrook; Mara was also contracted for a sequel. Bayer describes Nancy as "the loneliest girl in the world". Mara stated that her Nancy is different from the role of Nancy Thompson, performed by Heather Langenkamp, and described her character as "socially awkward and timid and really doesn\'t know how to connect with people". Kyle Gallner was cast as Quentin, who forms a connection with Nancy. Gallner described his character as "a mess, more jittery and more \'out there\' than Nancy is". Gallner pointed out that his character is like this because of the amount of pharmaceuticals he ingests to stay awake. Producer Brad Fuller commented that Gallner brought a sense of "humanity and relatability" to the role with his compassion and intellect. Other cast members include Katie Cassidy, Thomas Dekker, and Kellan Lutz. Cassidy performed the role of Kris. According to Cassidy, Kris becomes an emotional wreck throughout the film. Cassidy described her character\'s ordeal: "She is literally dragged through hell, having to crawl through dark, claustrophobic tunnels. She\'s always crying and freaking out as her nightmares of Freddy bleed into her everyday life. Kris suspects there\'s something that connects her with the others; she even confronts her mother about it, but no one\'s talking."', 'references': 163, 'contributors': 1059, 'last_updated': '2024-02-16 19:55'}}

Claim: Paradise is an EP.
Label: SUPPORTS
Result: NOT ENOUGH INFO
Result details: {'result': 'no_result', 'explanation': 'No matching citations found.'}

Claim: Frank Zappa's debut album was released in 1966.
Label: SUPPORTS
Result: REFUTES
Result details: {'result': 'incorrect', 'explanation': "The provided sources mention that Zappa's earliest professional recordings were soundtracks for films in 1962 and 1965, not his debut album.", 'article': {'url': 'http://en.wikipedia.org/?curid=10672', 'page_url': 'http://en.wikipedia.org/?curid=10672', 'section_url': 'http://en.wikipedia.org/?curid=10672#Lead_Section', 'title': 'Frank Zappa', 'section': 'Lead Section', 'snippet': "Zappa's earliest professional recordings, two soundtracks for the low-budget films The World's Greatest Sinner (1962) and Run Home Slow (1965) were more financially rewarding.", 'references': 317, 'contributors': 3109, 'last_updated': '2024-03-18 20:41'}}

Claim: Blackhat stars an actor.
Label: SUPPORTS
Result: NOT ENOUGH INFO
Result details: {'result': 'no_result', 'explanation': 'No matching citations found.'}

Claim: The Houston Rockets have won four Western Conference titles.
Label: SUPPORTS
Result: REFUTES
Result details: {'result': 'incorrect', 'explanation': 'The selected passage does not mention the Houston Rockets winning four Western Conference titles. It discusses the Denver Nuggets advancing to their first NBA Finals, not the Houston Rockets winning Western Conference titles.', 'article': {'url': 'http://en.wikipedia.org/?curid=70868858', 'page_url': 'http://en.wikipedia.org/?curid=70868858', 'section_url': 'http://en.wikipedia.org/?curid=70868858#Conference_finals', 'title': '2023 NBA playoffs', 'section': 'Conference finals', 'snippet': 'The Nuggets exorcised their playoff demons against the Lakers and advanced to their first NBA Finals in their 47-year history.', 'references': 250, 'contributors': 288, 'last_updated': '2024-03-14 23:47'}}

Claim: Margot Kidder won an award in the 2010s.
Label: SUPPORTS
Result: NOT ENOUGH INFO
Result details: {'result': 'no_result', 'explanation': 'No matching citations found.'}

Claim: Syria includes Jewish people.
Label: NOT ENOUGH INFO
Result: SUPPORTS
Result details: {'result': 'correct', 'explanation': 'The text confirms the presence of Jewish people in Syria since ancient times.', 'article': {'url': 'http://en.wikipedia.org/?curid=5531820', 'page_url': 'http://en.wikipedia.org/?curid=5531820', 'section_url': 'http://en.wikipedia.org/?curid=5531820#Lead_Section', 'title': 'Syrian Jews', 'section': 'Lead Section', 'snippet': "There have been Jews in Syria since ancient times: according to the community's tradition, since the time of King David, and certainly since early Roman times.", 'references': 42, 'contributors': 900, 'last_updated': '2024-03-15 20:51'}}

Claim: Leonardo da Vinci is a painter.
Label: SUPPORTS
Result: REFUTES
Result details: {'result': 'incorrect', 'explanation': 'Leonardo da Vinci was not just a painter, but also a scientist, engineer, and inventor. He was known for his diverse talents and contributions in various fields.', 'article': {'url': 'http://en.wikipedia.org/?curid=18079', 'page_url': 'http://en.wikipedia.org/?curid=18079', 'section_url': 'http://en.wikipedia.org/?curid=18079#Lead_Section', 'title': 'Leonardo da Vinci', 'section': 'Lead Section', 'snippet': 'Leonardo da Vinci, properly named Leonardo di ser Piero da Vinci ("Leonardo, son of ser Piero from Vinci"), was born on 15 April 1452 in, or close to, the Tuscan hill town of Vinci, 20 miles from Florence.', 'references': 201, 'contributors': 4003, 'last_updated': '2024-03-17 09:18'}}

Claim: Laura Linney was in a movie.
Label: REFUTES
Result: NOT ENOUGH INFO
Result details: {'result': 'no_result', 'explanation': 'No matching citations found.'}

Claim: Audrey Hepburn starred in a movie.
Label: SUPPORTS
Result: REFUTES
Result details: {'result': 'incorrect', 'explanation': 'The passage does not mention Audrey Hepburn starring in a movie, but rather discusses a television movie about her.', 'article': {'url': 'http://en.wikipedia.org/?curid=3679431', 'page_url': 'http://en.wikipedia.org/?curid=3679431', 'section_url': 'http://en.wikipedia.org/?curid=3679431#Reception', 'title': 'The Audrey Hepburn Story', 'section': 'Reception', 'snippet': 'Reception\nCritical reviews noted that the film overcame several potential pitfalls, including the usual insipidness of television movies and the difficulty of mounting a biopic of a revered actress who had died only seven years earlier.', 'references': 12, 'contributors': 121, 'last_updated': '2024-03-05 12:58'}}

Claim: Edge of Tomorrow was released in Portugal.
Label: NOT ENOUGH INFO
Result: REFUTES
Result details: {'result': 'incorrect', 'explanation': "The text does not mention Portugal as one of the countries where Disney's movies were released through local distributors.", 'article': {'url': 'http://en.wikipedia.org/?curid=1475269', 'page_url': 'http://en.wikipedia.org/?curid=1475269', 'section_url': 'http://en.wikipedia.org/?curid=1475269#International_distribution', 'title': 'Walt Disney Studios Motion Pictures', 'section': 'International distribution', 'snippet': "In some other countries in Europe, such as Poland, Hungary and the Czech Republic, Disney's movies were instead released through local distributors, such as Filmoteka Narodowa in Poland, InterCom Zrt. in Hungary and Guild Film Distribution in the Czech Republic respectively.", 'references': 193, 'contributors': 1055, 'last_updated': '2024-03-17 20:49'}}

Claim: Her was given a wide release on January 10, 2014.
Label: SUPPORTS
Result: NOT ENOUGH INFO
Result details: {'result': 'no_result', 'explanation': 'No matching citations found.'}

Claim: Moana is a 2016 novel.
Label: REFUTES
Result: NOT ENOUGH INFO
Result details: {'result': 'no_result', 'explanation': 'No matching citations found.'}

Claim: Just My Luck (2006 film) features Chris Pine in a lead role.
Label: SUPPORTS
Result: NOT ENOUGH INFO
Result details: {'result': 'no_result', 'explanation': 'No matching citations found.'}

Claim: Justin Bieber released at least four studio albums.
Label: SUPPORTS
Result: REFUTES
Result details: {'result': 'incorrect', 'explanation': 'There are matching sources to the claim', 'article': {'url': 'http://en.wikipedia.org/?curid=26023649', 'page_url': 'http://en.wikipedia.org/?curid=26023649', 'section_url': 'http://en.wikipedia.org/?curid=26023649#Lead_Section', 'title': 'Justin Bieber discography', 'section': 'Lead Section', 'snippet': 'itions Sales Certifications CAN AUS DEN FRA GER NLD  NZ SWE UK US My World 2.0 Released: March 19, 2010\n Label: Island, Teen Island, RBMG, Schoolboy\n Formats: CD, LP, digital download, streaming 1  1  7  4  7  4  1  17  3  1 UK: 827,000\n US: 3,400,000 MC: 3× Platinum\n ARIA: 3× Platinum\n BPI: Silver\n NVPI: Platinum', 'references': 424, 'contributors': 2091, 'last_updated': '2024-03-09 19:49'}}

Claim: 100 Greatest of All Time first premiered on the Tennis Channel.
Label: SUPPORTS
Result: REFUTES
Result details: {'result': 'incorrect', 'explanation': "The claim is incorrect as there is no mention of the show '100 Greatest of All Time' premiering on the Tennis Channel in the provided text.", 'article': {'url': 'http://en.wikipedia.org/?curid=3484304', 'page_url': 'http://en.wikipedia.org/?curid=3484304', 'section_url': 'http://en.wikipedia.org/?curid=3484304#Lead_Section', 'title': 'Tennis Channel', 'section': 'Lead Section', 'snippet': 'In 2001, Tennis Channel was founded by Steve Bellamy in the shed in his backyard, who soon hired Bruce Rider to head up programming and marketing.', 'references': 44, 'contributors': 209, 'last_updated': '2024-03-11 18:01'}}

Claim: Jonathan Hensleigh directed Jumanji.
Label: REFUTES
Result: NOT ENOUGH INFO
Result details: {'result': 'no_result', 'explanation': 'No matching citations found.'}

Claim: Faith Evans is a person.
Label: SUPPORTS
Result: NOT ENOUGH INFO
Result details: {'result': 'no_result', 'explanation': 'No matching citations found.'}

Claim: There was a musician in the Grand Ole Pry.
Label: SUPPORTS
Result: NOT ENOUGH INFO
Result details: {'result': 'no_result', 'explanation': 'No matching citations found.'}

Claim: Islam does not have any followers.
Label: REFUTES
Result: NOT ENOUGH INFO
Result details: {'result': 'no_result', 'explanation': 'No matching citations found.'}

Claim: The United Kingdom is the 21st-most populous city.
Label: REFUTES
Result: NOT ENOUGH INFO
Result details: {'result': 'no_result', 'explanation': 'No matching citations found.'}

Claim: The Miami Marlins are a team that plays baseball.
Label: SUPPORTS
Result: NOT ENOUGH INFO
Result details: {'result': 'no_result', 'explanation': 'No matching citations found.'}

Claim: Birth of the Dragon comes out in summer 2017.
Label: SUPPORTS
Result: NOT ENOUGH INFO
Result details: {'result': 'no_result', 'explanation': 'No matching citations found.'}

Claim: Emily Ratajkowski was in a commercial for an upscale car brand during a Super Bowl.
Label: SUPPORTS
Result: NOT ENOUGH INFO
Result details: {'result': 'no_result', 'explanation': 'No matching citations found.'}

Claim: Sleeping Beauty premiered in May 2011.
Label: SUPPORTS
Result: NOT ENOUGH INFO
Result details: {'result': 'no_result', 'explanation': 'No matching citations found.'}

Claim: Beauty Shop is an original work.
Label: REFUTES
Result: NOT ENOUGH INFO
Result details: {'result': 'no_result', 'explanation': 'No matching citations found.'}

Claim: Philadelphia contains 67 National Historic Landmarks.
Label: SUPPORTS
Result: REFUTES
Result details: {'result': 'incorrect', 'explanation': 'The number of National Historic Landmarks in Philadelphia is not mentioned in the provided text passages.', 'article': {'url': 'http://en.wikipedia.org/?curid=404013', 'page_url': 'http://en.wikipedia.org/?curid=404013', 'section_url': 'http://en.wikipedia.org/?curid=404013#Current_National_Historic_Landmarks', 'title': 'National Historic Landmark', 'section': 'Current National Historic Landmarks', 'snippet': 'There are 74 NHLs in the District of Columbia.', 'references': 12, 'contributors': 424, 'last_updated': '2024-01-20 17:22'}}

Claim: Eddie Redmayne appeared in a film.
Label: SUPPORTS
Result: NOT ENOUGH INFO
Result details: {'result': 'no_result', 'explanation': 'No matching citations found.'}

Claim: Amelia Earhart was Canadian.
Label: NOT ENOUGH INFO
Result: REFUTES
Result details: {'result': 'incorrect', 'explanation': 'The quote clearly states that Grace Muriel Earhart, also known as Muriel Earhart Morrissey, was born in Kansas City, which contradicts the claim that Amelia Earhart was Canadian.', 'article': {'url': 'http://en.wikipedia.org/?curid=63949384', 'page_url': 'http://en.wikipedia.org/?curid=63949384', 'section_url': 'http://en.wikipedia.org/?curid=63949384#Lead_Section', 'title': 'Muriel Earhart Morrissey', 'section': 'Lead Section', 'snippet': 'Grace Muriel Earhart was born in Kansas City on December 29, 1899, two years after her sister Amelia, to parents Edwin and Amy Earhart.', 'references': 12, 'contributors': 30, 'last_updated': '2024-02-10 03:34'}}

Claim: London is in Greater London.
Label: SUPPORTS
Result: REFUTES
Result details: {'result': 'incorrect', 'explanation': 'The City of London is separate from Greater London.', 'article': {'url': 'http://en.wikipedia.org/?curid=45367389', 'page_url': 'http://en.wikipedia.org/?curid=45367389', 'section_url': 'http://en.wikipedia.org/?curid=45367389#Local_government', 'title': 'Greater London', 'section': 'Local government', 'snippet': 'Greater London is divided into 32 London Boroughs, each governed by a London Borough council. The City of London has a unique government dating back to the 12th century and is separate from the county of Greater London, although is still part of the region served by the Greater London Authority.', 'references': 82, 'contributors': 950, 'last_updated': '2024-03-10 10:31'}}

Claim: The Eagles were active in the 1970s.
Label: SUPPORTS
Result: REFUTES
Result details: {'result': 'incorrect', 'explanation': 'The statement is incorrect as it refers to the period after the Eagles broke up, not during the 1970s when the band was active.', 'article': {'url': 'http://en.wikipedia.org/?curid=90785', 'page_url': 'http://en.wikipedia.org/?curid=90785', 'section_url': 'http://en.wikipedia.org/?curid=90785#Lead_Section', 'title': 'Eagles (band)', 'section': 'Lead Section', 'snippet': 'After the Eagles broke up, the former members pursued solo careers.', 'references': 145, 'contributors': 2829, 'last_updated': '2024-02-20 05:03'}}

Claim: The 2013 NBA draft was planned at Barclays Center in Brooklyn.
Label: NOT ENOUGH INFO
Result: SUPPORTS
Result details: {'result': 'correct', 'explanation': 'The quote directly confirms that the 2013 NBA draft was indeed planned at Barclays Center in Brooklyn.', 'article': {'url': 'http://en.wikipedia.org/?curid=1658814', 'page_url': 'http://en.wikipedia.org/?curid=1658814', 'section_url': 'http://en.wikipedia.org/?curid=1658814#Basketball', 'title': 'Barclays Center', 'section': 'Basketball', 'snippet': 'The venue hosted the NBA draft starting with the 2013 event on June 27, 2013 and it has been ever since, except the 2020 event was held via videoconferencing.', 'references': 131, 'contributors': 939, 'last_updated': '2024-03-18 21:27'}}

Claim: Mission: Impossible 6 is written by a person.
Label: SUPPORTS
Result: NOT ENOUGH INFO
Result details: {'result': 'no_result', 'explanation': 'No matching citations found.'}

Claim: Believe is a work.
Label: SUPPORTS
Result: REFUTES
Result details: {'result': 'incorrect', 'explanation': 'The claim is too vague and does not align with the content of the sources.', 'article': {'url': 'http://en.wikipedia.org/?curid=2969882', 'page_url': 'http://en.wikipedia.org/?curid=2969882', 'section_url': 'http://en.wikipedia.org/?curid=2969882#Lead_Section', 'title': 'Believe (Cher album)', 'section': 'Lead Section', 'snippet': "After Cher's poorly received previous record It's a Man's World (1995), head of Warner Music UK Rob Dickins suggested that Cher record a dance album that could appeal to her gay audience.", 'references': 147, 'contributors': 528, 'last_updated': '2024-03-13 17:27'}}

Claim: Russ is a rapper.
Label: SUPPORTS
Result: NOT ENOUGH INFO
Result details: {'result': 'no_result', 'explanation': 'No matching citations found.'}

Claim: Wilt Chamberlain plays basketball.
Label: SUPPORTS
Result: NOT ENOUGH INFO
Result details: {'result': 'no_result', 'explanation': 'No matching citations found.'}

Claim: One Russian superhero is Black Widow.
Label: NOT ENOUGH INFO
Result: SUPPORTS
Result details: {'result': 'partially_correct', 'explanation': 'The claim is partially correct as there are multiple Russian superheroes with the name Black Widow, including Natasha Romanoff and Yelena Belova.', 'article': {'url': 'http://en.wikipedia.org/?curid=6129671', 'page_url': 'http://en.wikipedia.org/?curid=6129671', 'section_url': 'http://en.wikipedia.org/?curid=6129671#Marvel_Comics', 'title': 'List of Russian superheroes', 'section': 'Marvel Comics', 'snippet': 'Black Widow (Natasha Romanoff of the Avengers)', 'references': 26, 'contributors': 155, 'last_updated': '2024-02-13 19:11'}}

Claim: Captain America: The Winter Soldier is a work.
Label: SUPPORTS
Result: NOT ENOUGH INFO
Result details: {'result': 'no_result', 'explanation': 'No matching citations found.'}

Claim: Rhona Mitra acts.
Label: SUPPORTS
Result: NOT ENOUGH INFO
Result details: {'result': 'no_result', 'explanation': 'No matching citations found.'}

Claim: The Matrix stars Keanu Reeves as John Matrix.
Label: SUPPORTS
Result: NOT ENOUGH INFO
Result details: {'result': 'no_result', 'explanation': 'No matching citations found.'}
